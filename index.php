<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

use justjob\controller\AjoutCategorieController;
use justjob\controller\ConnexionCompteController;
use justjob\controller\CreationCompteController;
use justjob\controller\HomePageController;
use justjob\controller\CreationOffreController;
use justjob\controller\ModificationCompteController;
use justjob\controller\OffreController;
use justjob\controller\OffredetailController;
use justjob\controller\ListeCandidatureController;
use justjob\controller\CompteListeController;
use justjob\controller\RoleController;
use justjob\controller\SupprimerCategorieController;
use justjob\controller\TransportsController;
use justjob\controller\UtilisateurController;

use justjob\controller\ModifierCategorieController;

use justjob\controller\CandidatureDetailController;

use justjob\controller\ModificationCategorieController;


require 'vendor/autoload.php';

session_start();

$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


$app = new \Slim\Slim();


$app->get('/', function () {
    HomePageController::afficher();
})->name("accueil");


$app->post('/', function () {
    HomePageController::afficher();
});

$app->get('/creationCompte', function () {
    CreationCompteController::afficher();
})->name('authentification');

$app->get('/connexionCompte', function () {
    ConnexionCompteController::afficher();
})->name('connexion');


$app->get('/creationOffre', function () {
    CreationOffreController::afficher();
})->name("creationOffre");

$app->get('/offre', function () {
    OffreController::afficher();
})->name("offre");

$app->get('/offredetail', function () {
    OffredetailController::afficher();
})->name("offredetail");

$app->get('/listeCandidature', function () {
    ListeCandidatureController::afficher();
})->name("listeCandidature");

$app->get('/modificationCompte', function () {
    ModificationCompteController::afficher();
})->name("compte");


$app->get('/roleCompte', function () {
    RoleController::afficher();
})->name("roleCompte");

$app->get('/compteListe', function () {
    CompteListeController::afficher();
})->name("compteListe");

$app->get('/utilisateurs', function () {
    UtilisateurController::afficher();
})->name("utilisateurs");


$app->get('/candidature:id', function ($id) {
    CandidatureDetailController::afficher($id);
})->name("candidature");

$app->get('/ajoutCateg', function () {
    AjoutCategorieController::afficher();
})->name("ajoutCateg");

$app->get('/modifierCateg', function () {
    ModifierCategorieController::afficher();
})->name("modifCateg");

$app->get('/supprimerCateg', function () {
    SupprimerCategorieController::afficher();
})->name("supprimerCateg");

$app->get('/transports', function () {
    TransportsController::afficher();
})->name("transports");

$app->get('/listePropositions', function () {
    \justjob\controller\ListeTransportsController::afficher();
})->name("listeProposition");

$app->run();

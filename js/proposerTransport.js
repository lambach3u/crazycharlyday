function proposerTransport(idcandid, iduser) {
    $.ajax({
        url: "src/controller/TransportsController.php",
        type: "POST",
        data: {
            candid_id: idcandid,
            user_id: iduser
        },
        dataType: "text",
        success: function (r) {
            console.log(r);
            if (r === "1")
                location.href = "transports";
        },
        error: function (r) {
            console.log(r);
        }
    });
}
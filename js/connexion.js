/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

$("#connectButton").on("click", function () {
    var nom = document.getElementById("inputNom").value;
    var password = document.getElementById("inputPassword").value;

    $.ajax({
        url: "src/controller/ConnexionCompteController.php",
        type: "POST",
        data: {
            nom: nom,
            password: password
        },
        dataType: "text",
        success: function (r) {
            if (r === "1") {
                console.log("success");
                console.log(r);
                location.href = ".";
            }
        }, error: function (r) {
            console.log("error");
            console.log(r);
        }
    });
});

$("#buttonCandidater").on("click", function () {
    console.log("coucou");
    $.ajax({
        url: "src/controller/OffredetailController.php",
        type: "POST",
        data: {"candidater": ""},
        dataType: "text",
        success: function (r) {
            console.log(r);
            if (r === "1") {
                showCandidature();
            }
        },
        error: function (r) {
            console.log(r);
        }
    });
});

$("#validerCandidat").on("click", function () {
    var depart = document.getElementById("startLocation").value;
    var transport = document.getElementById("transportSelect");
    var descr = document.getElementById("description").value;
    var transportOption = transport.options[transport.selectedIndex].value;
    console.log(depart);
    console.log(transportOption);
    $.ajax({
        url: "src/controller/OffredetailController.php",
        type: "POST",
        data: {
            depart: depart,
            description: descr,
            transport: transportOption
        },
        dataType: "text",
        success: function (r) {
            console.log(r);
            if (r === "1") {
                location.href = "offre";
            }
        },
        error: function (r) {
            console.log(r);
        }
    });
});

function showCandidature() {
    console.log("showing");
    document.getElementById("candidature").style.display = "block";
}

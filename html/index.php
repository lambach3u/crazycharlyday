<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="Jobboard">

    <title>JustJob</title>


    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/jasny-bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/bootstrap-select.min.css" type="text/css">
    <!-- Material CSS -->
    <link rel="stylesheet" href="assets/css/material-kit.css" type="text/css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="assets/fonts/themify-icons.css">

    <!-- Animate CSS -->
    <link rel="stylesheet" href="assets/extras/animate.css" type="text/css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="assets/extras/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/extras/owl.theme.css" type="text/css">
    <!-- Rev Slider CSS -->
    <link rel="stylesheet" href="assets/extras/settings.css" type="text/css">
    <!-- Editor -->
    <link rel="stylesheet" href="assets/extras/froala_editor.min.css" type="text/css">
    <!-- Slicknav js -->
    <link rel="stylesheet" href="assets/css/slicknav.css" type="text/css">
    <!-- Main Styles -->
    <link rel="stylesheet" href="assets/css/main.css" type="text/css">
    <!-- Responsive CSS Styles -->
    <link rel="stylesheet" href="assets/css/responsive.css" type="text/css">

    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="assets/css/colors/red.css" media="screen"/>


</head>

<body>
<!-- Header Section Start -->
<div class="header">
    <section id="intro" class="
        <?php
    if ($_SESSION['page'] == 'homePage') {
        echo "section-intro";
    } else {
        echo "";
    }
    ?>">
        <div class="logo-menu">
            <nav class="navbar navbar-default main-navigation" role="navigation" data-spy="affix" data-offset-top="50">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logo" href="."><img src="assets/img/logo.png" alt=""
                                                                   style="width: 4em;height: 4em"></a>
                    </div>

                    <div class="collapse navbar-collapse" id="navbar">
                        <!-- Start Navigation List -->
                        <ul class="nav navbar-nav">
                            <li>
                                <a href=".">
                                    Accueil
                                </a>

                            </li>
                            <li>
                                <a href="<?php echo \Slim\Slim::getInstance()->urlFor('offre'); ?>">Offres</a>
                            </li>
                            <li>
                                <a href="#">
                                    Candidatures <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown">
                                    <li>
                                        <a href="listeCandidature">
                                            Affichez vos candidatures
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="" href="#">
                                    Employeurs <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown">
                                    <li>
                                        <a class="" href="creationOffre">
                                            Ajouter une offre d'emploi
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php
                            if (isset($_SESSION["user"])) {
                                $url = \Slim\Slim::getInstance()->urlFor('utilisateurs');
                                $html = <<<END
                            <li>
                            <a href="$url">
                                Utilisateurs
                            </a>

                        </li>
END;
                       if(isset($_SESSION["user"]))
                           echo "
                        <li>
                            <a href=\"#\">
                                Transports <i class=\"fa fa-angle-down\"></i>
                            </a>
                            <ul class=\"dropdown\">
                                    <li>
                                        <a class=\"\" href=\"listePropositions\">
                                            Mes propositions
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"transports\">
                                            Tous les transports
                                        </a>
                                    </li>
                             </ul>
                        </li>";
                                if ($_SESSION["user"]["role"] == "Admin" || $_SESSION["user"]["role"] == "Super-Admin") {
                                    $html = $html . <<<END
                                <li>
                                <a class="" href="#">
                                    Catégories <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown">
                                    <li>
                                        <a class="" href="ajoutCateg">
                                            Ajouter une catégorie
                                        </a>
                                    </li>
                                    <li>
                                        <a href="modifierCateg">
                                            Modifier une catégorie
                                        </a>
                                    </li>
                                    <li>
                                        <a href="supprimerCateg">
                                            Supprimer une catégorie
                                        </a>
                                    </li>                                   
                                </ul>
                            </li>
END;
                                }


                                echo $html;
                            }
                            ?>

                        </ul>
                        <ul class="nav navbar-nav navbar-right float-right">
                            <?php
                            if (!isset($_SESSION["user"])) {
                                echo "<li class=\"left\"><a href=" . \Slim\Slim::getInstance()->urlFor("compteListe") . "><i
                                        class=\"ti-arrow-right\"></i>Compte Liste</a></li>";
                            } else if ($_SESSION["user"]["role"] == "Admin" || $_SESSION["user"]["role"] == "Super-Admin") {
                                echo "<li class=\"left\"><a href=" . \Slim\Slim::getInstance()->urlFor("roleCompte") . "><i
                                        class=\"ti-arrow-right\"></i>Rôles</a></li>";
                            }
                            if (!isset($_SESSION["user"]))
                                echo "
                        <li class=\"left\"><a href=" . \Slim\Slim::getInstance()->urlFor("authentification") . "><i
                                        class=\"ti-pencil-alt\"></i>S'inscrire</a></li>";
                            else
                                echo "
                        <li class=\"left\"><a href=" . \Slim\Slim::getInstance()->urlFor("compte") . "><i
                                        class=\"ti-pencil-alt\"></i>Mon compte</a></li>";
                            ?>


                            <?php
                            if (!isset($_SESSION["user"]))
                                echo "
                        <li class=\"right\"><a href=" . \Slim\Slim::getInstance()->urlFor('connexion') . "><i
                                class=\"ti-lock\"></i>Connexion</a></li>";
                            else {
                                echo "
                        <li class=\"right\"><a onclick=\"deconnexion()\"><i class=\"ti-lock\"></i>Déconnexion</a></li>
                            ";
                                $lien = "ccd2019-bdd/img/" . $_SESSION['user']->id . ".jpg";
                                if (!file_exists($lien))
                                    $lien = "ccd2019-bdd/img/unknown.jpg";
                                echo "<li class=\"right\"><img src=\"" . $lien . "\" style='height: 4em'></i></li>";
                            }
                            ?>
                        </ul>
                    </div>
                </div>

            </nav>
        </div>
        <!-- Header Section End -->
        <!-- Page Header Start -->
        <div class="page-header" style="background: url(assets/img/banner1.jpg);visibility: <?php
        if ($_SESSION['page'] == 'homePage') {
            echo "hidden";
        } else {
            echo "visible";
        }
        ?>
                ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumb-wrapper">
                            <h2 class="product-title">
                                <?php
                                if (isset($_SESSION['title'])) {
                                    echo $_SESSION['title'];
                                } else {
                                    echo "Just Job";
                                }
                                ?></h2>
                            <ol class="breadcrumb">
                                <!--<li><a href="#"><i class="ti-home"></i> Home</a></li>-->
                                <!--<li class="current">Post A Job</li>-->
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page Header End -->

        <section class="section text-center">
            <div class="container">
                <?php
                if (isset($_SESSION['body'])) {
                    echo $_SESSION['body'];
                }
                ?>
            </div>
        </section>
    </section>


    <!-- Footer Section Start -->
    <footer>
        <!-- Footer Area Start -->
        <section class="footer-Content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="widget">
                            <h3 class="block-title"><img src="assets/img/logo.png" class="img-responsive"
                                                         alt="Footer Logo"></h3>
                            <div class="textwidget">

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="widget">
                            <h3 class="block-title">Liens utiles</h3>
                            <ul class="menu">
                                <li><a href="connexionCompte">Connexion</a></li>
                                <li><a href="creationCompte">Inscription</a></li>
                                <li><a href="compteListe">Liste de comptes</a></li>
                                <li><a href="listeCandidature">Candidatures</a></li>
                                <li><a href="offre">Offres</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="widget">
                            <h3 class="block-title">Retrouvez-nous sur les réseaux sociaux :</h3>
                            <div class="bottom-social-icons social-icon">
                                <a class="twitter" href="https://twitter.com/iutcharlemagne"><i
                                            class="ti-twitter-alt"></i></a>
                                <a class="facebook" href="https://www.facebook.com/iutcharlemagne/"><i
                                            class="ti-facebook"></i></a>
                                <a class="youtube"
                                   href="https://www.youtube.com/channel/UCTPh8EbmKhRwj60D_V1KoMg/featured"><i
                                            class="ti-youtube"></i></a>
                                <a class="linkedin" href="https://www.linkedin.com/in/iut-nancy-charlemagne/"><i
                                            class="ti-linkedin"></i></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer area End -->

        <!-- Copyright Start  -->
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="site-info text-center">
                            <p>All Rights reserved &copy; 2019
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Copyright End -->

    </footer>
    <!-- Footer Section End -->

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
        <i class="ti-arrow-up"></i>
    </a>

    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
                <div class="object" id="object_five"></div>
                <div class="object" id="object_six"></div>
                <div class="object" id="object_seven"></div>
                <div class="object" id="object_eight"></div>
            </div>
        </div>
    </div>

    <!-- Main JS  -->
    <script type="text/javascript" src="assets/js/jquery-min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="assets/js/jquery.slicknav.js"></script>
    <script type="text/javascript" src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/js/jquery.counterup.min.js"></script>
    <script type="text/javascript" src="assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="assets/js/jasny-bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="assets/js/form-validator.min.js"></script>
    <script type="text/javascript" src="assets/js/contact-form-script.js"></script>


</body>
</html>
<script type="text/javascript" src="js/<?php if (isset($_SESSION["script"])) echo $_SESSION["script"]; ?>"></script>
<script>
    function deconnexion() {
        $.ajax({
            url: "src/controller/HomePageController.php",
            type: "POST",
            data: {
                deconnexion: 1
            },
            dataType: "text",
            success: function (r) {
                console.log("success");
                console.log(r);
                location.href = ".";
            }, error: function (r) {
                console.log("error");
                console.log(r);
            }
        });
    }
</script>
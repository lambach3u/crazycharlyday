<?php
/**
 * Projet crazycharlyday
 * 2018 - 2019
 * Created by machilus
 */


namespace justjob\vue;

use justjob\models\CandidatureEmploi;
use justjob\models\OffreEmploi;
use justjob\models\PropositionTransport;

class VueTransports extends Vue {
    function ajouter() {
        if (isset($_SESSION["user"]))
            $iduser = $_SESSION["user"]->id;
        else
            $iduser = -1;
        $_SESSION["body"] = <<<END
        <!-- Find Job Section Start -->
    <section class="find-job section">
      <div class="container">
        <h2 class="section-title">Tous les transports</h2>
        <div class="row">
          <div class="col-md-12">
END;
        $candidatures = CandidatureEmploi::where("besoin_transport", "=", 1)->get();
        foreach ($candidatures as $candidature) {

            $idoffre = $candidature->id_offre;
            $offre = OffreEmploi::where("id", "=", $idoffre)->first();
            if ($offre != null)
                $lieuDepart = $offre->lieu;
            else
                continue;

            $lieuArrivee = $candidature->depart;

            if ($candidature->etat == 0)
                $etatCandidature = "En attente.";
            else if ($candidature->etat == 1)
                $etatCandidature = "Acceptée.";
            else if ($candidature->etat == 2)
                $etatCandidature = "Refusée.";
            else
                continue;

            $_SESSION["body"] .= <<<END
            <div class="job-list">

              <div class="job-list-content">
                <h4>$lieuArrivee -> $lieuDepart</h4>
                <p>Vehicule : $candidature->type_vehicule</p>
                <p>Candidature : $etatCandidature</p>
END;
            if ($iduser != -1 && $candidature->id_employe != $iduser && PropositionTransport::where("id_user", "=", $iduser)->where("id_candidature", "=", $candidature->id)->first() == null)
                $_SESSION["body"] .= "
                <button class=\"btn btn-success\" onclick=\"proposerTransport($candidature->id, $iduser)\">Proposer un transport</a>";
            $_SESSION["body"] .= <<<END
              </div>
            </div>
END;
        }
        $_SESSION["body"] .= <<<END
        </div>
      </div>
    </section>
END;
        $_SESSION["script"] = "proposerTransport.js";
    }

    function render() {
        parent::render();
    }
}
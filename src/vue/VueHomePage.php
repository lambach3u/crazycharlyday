<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\vue;

use justjob\models\User;

class VueHomePage extends Vue {

    function __construct() {
        parent::__construct();
    }

    function ajouter() {

            $_SESSION["body"] = <<<END
<div class="search-container">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h1>Trouvez le travail de vos rêves</h1><br>
              <div class="content">
                <form method="" action="">
                  <div class="row">
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <input class="form-control" type="text" placeholder="poste  / mots clés / nom d'entreprise">
                        <i class="ti-time"></i>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                      <div class="form-group">
                        <input class="form-control" type="email" placeholder="ville / province / code postal">
                        <i class="ti-location-pin"></i>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                      <div class="search-category-container">
                        <label class="styled-select">
                          <select class="dropdown-product selectpicker">
                            <option>Tous les Catégories</option>
                           
                          </select>
                        </label>
                      </div>
                    </div>
                    <div class="col-md-1 col-sm-6">
                      <button type="button" class="btn btn-search-icon"><i class="ti-search"></i></button>
                    </div>
                  </div>
                </form>
              </div>
              
            </div>
          </div>
        </div>
      </div>
END;


    }

    function render() {
        parent::render();
    }
}
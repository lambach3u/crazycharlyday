<?php
/**
 * Projet crazycharlyday
 * 2018 - 2019
 * Created by machilus
 */


namespace justjob\vue;


use justjob\models\Categorie;
use justjob\models\OffreEmploi;

class VueOffre extends Vue {

    function ajouter() {
        $_SESSION['page']='offre';
        $_SESSION["body"] = <<<END
        <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1k9qr_CZpYBqWZhILJ3H9No_qn9ZO9jFv" width="640" height="480"></iframe>
        <!-- Find Job Section Start -->
    <section class="find-job section">
      <div class="container">
        <h2 class="section-title">Offres</h2>
        <div class="row">
          <div class="col-md-12">
END;
        $offres=OffreEmploi::orderBy('id', 'DESC')->get();
        foreach ($offres as $offre) {
            $id=$offre->id;
            $title=$offre->intitule;
            $description=$offre->description;
            $lieu=$offre->lieu;

            $cate=Categorie::where('id','=',$offre->id_categorie)->first()->nom;
            $_SESSION["body"] .= <<<END
<div class="job-list">

              <div class="job-list-content">
                <h4><a>$title</a></h4>
                <p>$description</p>
                <div class="job-tag">
                  <div class="pull-left">
                    <div class="meta-tag">
                      <span><a>$cate</span>
                      <span><i class="ti-location-pin"></i>$lieu France</span>
                    </div>
                  </div>
                  <div class="pull-right">
                    <a class="btn btn-common btn-rm" onclick="offre('$id')">Plus d'infos</a>
                  </div>
                </div>
              </div>
            </div>
END;
        }

        $_SESSION["body"].= <<<END
        </div>
      </div>
    </section>
END;
        $_SESSION["script"] = "offre.js";
    }

    function render() {
        parent::render();
    }
}
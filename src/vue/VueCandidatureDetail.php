<?php
/**
 * Projet JustJob
 * 2018 - 2019
 * Created by Theau
 */


namespace justjob\vue;


use justjob\controller\CandidatureDetailController;
use justjob\controller\ListeCandidatureController;
use justjob\models\CandidatureEmploi;

class VueCandidatureDetail extends Vue {

    private $candid;

    private $type;
    private $intitule;

    private $description;

    private $forbidden;

    function __construct($id = -1){
        parent::__construct();
        if ($id !== -1){
            $this->candid = CandidatureDetailController::getCandidature($id);

            // $this->type = CandidatureDetailController::getCandidature()->type_vehicule;
            $this->intitule = CandidatureDetailController::getIntitule($id);
            $this->description = CandidatureDetailController::getDescription($id);
            $this->forbidden = 0;
        } else {
            $this->forbidden = 1;
        }
    }

    function ajouter(){
        if ($this->forbidden === 1){
            $_SESSION["body"] = "
            <h1>Aucune candidature trouvée</h1>
        ";
        } else {
            $_SESSION["body"] = "
            <h1>Réponse à l'offre <br> $this->intitule</h1>
            <p>$this->description</p>
        ";
        }
        $_SESSION["script"] = "creationOffre.js";

    }

    function render(){
        parent::render();
    }
}
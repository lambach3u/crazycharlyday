<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\vue;


use justjob\models\User;

class VueRole extends Vue {

    function __construct() {
        parent::__construct();
    }

    function ajouter() {
        $body = <<<END
        <form>
            <label for="select1">Utilisateur :</label>
            <select class="form-control" id="select1">
                <option></option>
END;

        $users = User::all();
        foreach ($users as $value) {
            if ($value->nom != $_SESSION["user"]["nom"] && $value->role != "Super-Admin") {


                $body = $body . <<<END
                <option>$value->nom</option>
END;
            }
        }

        $body = $body . <<<END
            </select>
END;

        $body = $body . <<<END
            <label for="select2">Rôle à donner :</label>
            <select class="form-control" id="select2">
                <option></option>
                <option>Utilisateur</option>
                <option>Admin</option>
            </select>
            <button id="brole" type="button" class="btn btn-primary">Attribuer</button>
        </form>
END;

        $_SESSION["body"] = $body;
        $_SESSION["script"] = "role.js";
    }

    function render() {
        parent::render();
    }

}
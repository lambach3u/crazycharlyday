<?php
/**
 * Projet crazycharlyday
 * 2018 - 2019
 * Created by Theau
 */


namespace justjob\vue;


use justjob\controller\ListeTransportsController;

class VueListeTransports extends Vue {

    private $items;

    function __construct($id = -1){
        parent::__construct();

        $this->items = ListeTransportsController::listPropositions();
    }

    function ajouter(){
        $_SESSION["body"] = "
            <h2 style='margin-bottom: 1em;'>Mes transports</h2>
            <div>
                $this->items
            </div>
            ";
        $_SESSION["script"] = "creationOffre.js";
    }

    function render(){
        parent::render();
    }

}
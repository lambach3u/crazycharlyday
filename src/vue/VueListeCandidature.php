<?php
/**
 * Projet JustJob
 * 2018 - 2019
 * Created by Theau
 */


namespace justjob\vue;


use justjob\controller\ListeCandidatureController;

class VueListeCandidature extends Vue {

    private $candid;
    private $answer;

    private $candidDiv;
    private $answerDiv;

    function __construct(){
        parent::__construct();

        $this->candid = ListeCandidatureController::listCandid();
        $this->answer = ListeCandidatureController::listAnswers();

        if (ListeCandidatureController::hasSentCandid()){
            $this->candidDiv = "
            <div class=\"dropdown\">
            <h1 style='text-align: center; margin-bottom: 1em'>Vos candidatures</h1>
            <div class='list-group'>
                $this->candid
            </div>
            </div>
            ";

        } else {
            $this->candidDiv = "
            <div class=\"dropdown\">
            <h1 style='text-align: center; margin-bottom: 1em'>Vos candidatures</h1>
            <div style='text-align: center; margin-bottom: 1em'>
                Vous n'avez envoyé aucune candidature !
                </div>
            </div>
            ";
        }

        if (ListeCandidatureController::hasReceivedCandid()){
            $this->answerDiv = "
            <div class=\"dropdown\">
            <h1 style='text-align: center; margin-bottom: 1em; margin-top: 1em;'>Réponses à vos offres</h1>
            <div class='list-group'>
                $this->answer
            </div>
            </div>
            ";
        } else {
            $this->answerDiv = "
            <div class=\"dropdown\">
            <h1 style='text-align: center; margin-bottom: 1em; margin-top: 1em;'>Réponses à vos offres</h1>
            <div style='text-align: center; margin-bottom: 1em'>
                Vous n'avez reçu aucune candidature !
                </div>
            </div>
            ";
        }

    }

    function ajouter(){
        $_SESSION["body"] = $this->candidDiv . $this->answerDiv;

        $_SESSION["script"] = "creationOffre.js";

    }

    function render(){
        parent::render();
    }
}
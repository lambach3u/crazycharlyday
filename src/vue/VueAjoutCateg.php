<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\vue;


class VueAjoutCateg extends Vue {

    function __construct() {
        parent::__construct();
    }

    function ajouter() {
        $_SESSION["body"] = <<<END
        <form>
        
          <div class="form-group">
            <label for="categ">Nouvelle catégorie :</label>
            <input type="text" class="form-control" id="newCateg" aria-describedby="nom" placeholder="Entrer la nouvelle catégorie">
          </div>
         
          <button id="baddcateg" type="button" class="btn btn-primary">Ajouter</button>
        </form>
END;

        $_SESSION["script"] = "ajoutcateg.js";
    }

    function render() {
        parent::render();
    }

}
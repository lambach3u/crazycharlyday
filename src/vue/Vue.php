<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\vue;

class Vue {
    function __construct() {
        $_SESSION['body'] = "";
    }

    public function render() {
        include('./html/index.php');
    }

} 
<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\vue;

class VueConnexionCompte extends Vue {

    function __construct() {
        parent::__construct();
    }

    function ajouter() {
        $_SESSION["body"] = <<<END
<link rel="stylesheet" type="text/css" href="./css/connexion.css">
 <div class="container">
    <div class="row">
      <div class="col-md-4 col-md-offset-5">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Connexion</h5>
            <form class="form-signin">
              <div class="form-label-group">
                <input type="text" id="inputNom" class="form-control" placeholder="Nom">
              </div>

              <div class="form-label-group">
                <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe">
              </div>

              <button class="btn btn-lg btn-primary btn-block" id="connectButton" type="button">Connexion</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
END;
        $_SESSION["script"] = "connexion.js";
    }

    function render() {
        parent::render();
    }
}
<?php
/**
 * Projet JustJob
 * 2018 - 2019
 * Created by Theau
 */


namespace justjob\vue;


use justjob\controller\CreationOffreController;

class VueCreationOffre extends Vue {

    public $choices;

    function __construct(){
        parent::__construct();

        $this->choices = CreationOffreController::listCategories();
    }

    function ajouter(){
        if (!isset($_SESSION["user"])){
            $_SESSION["body"] = "<h2>Vous n'avez pas le droit d'accéder à cette fonctionnalité. Vous devez être connecté.</h2>";
        } else {
            $_SESSION["body"] = "
        
        <div class=\"dropdown\">
            <button id=\"title\" class=\"btn btn-primary dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\">Choisir votre domaine
            <span class=\"caret\"></span></button>
            <ul id=\"choice\" class=\"dropdown-menu\" style=\"margin-left: 32em\">
                $this->choices
            </ul>
        </div>
        
        <div class=\"form-group\" style='margin-top: 2em'>
            <label >Lieu :</label>
            <input id=\"lieu\" type=\"text\" class=\"form-control\">
        </div>
        <div class=\"form-group\">
            <label >Profil :</label>
            <input id=\"profil\" type=\"text\" class=\"form-control\">
        </div>
        
        <div class=\"form-group\">
            <label >Durée :</label>
            <input id=\"duree\" type=\"text\" class=\"form-control\">
        </div>
        
        <div>
            <button onclick=\"validate()\" type=\"button\" class=\"btn btn-primary\">Valider</button>
        </div>
        ";
        }

        $_SESSION["script"] = "creationOffre.js";

    }

    function render(){
        parent::render();
    }

    function addChoices($string){
        $this->choices .= $string;
    }

}
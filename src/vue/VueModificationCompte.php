<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\vue;


class VueModificationCompte extends Vue {

    function __construct() {
        parent::__construct();
    }

    function ajouter() {
        $n = $_SESSION["user"]["nom"];
        $v = $_SESSION["user"]["ville"];
        $_SESSION["body"] = <<<END
        
        
        <form>
                 
          <div class="form-group">
            <label for="modifNewNom">Nouveau nom</label>
            <input type="text" class="form-control" id="nomM" aria-describedby="nom" value=$n>
          </div>
                 
          <div class="form-group">
            <label for="modifNewMdp">Nouveau mot de passe</label>
            <input type="password" class="form-control" id="passwordM" placeholder="Entrer un nouveau mot de passe">
          </div>
          
          <div class="form-group">
            <label for="exampleInputPassword1">Ville</label>
            <input type="text" class="form-control" id="villeM" value=$v>
          </div>
          <button id="bmodification" type="button" class="btn btn-primary">Modifier</button>
        </form>
END;
        $_SESSION["script"] = "modification.js";
    }

    function render() {
        parent::render();
    }

}
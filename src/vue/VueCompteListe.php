<?php
/**
 * Projet JustJob
 * 2018 - 2019
 * Created by machilus
 */


namespace justjob\vue;


use justjob\models\User;

class VueCompteListe extends Vue {

    function __construct() {
        parent::__construct();
    }

    function ajouter() {
        $_SESSION['page']='compterlist';

            $_SESSION["body"] = <<<END
<!-- Featured Jobs Section Start -->
    <section class="featured-jobs section">
      <div class="container">
        <h2 class="section-title">
          Vous n'avez pas encore un compte ?
          <br> Choisir ci-dessous! 
        </h2>
        <div class="row">
END;
            $list = User::where('mdp', '=', "")->get();
            foreach ($list as $compte) {
                $nom = $compte->nom;
                $image = "" . $compte->id . ".jpg";
                $url = "ccd2019-bdd/img/" . $image;
                $_SESSION["body"] .= <<<END
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="featured-item">
              <div class="featured-wrap">
                <div class="featured-inner">
                <figure class="item-thumb">
                    <a class="hover-effect" href="job-page.html">
                      <img src="$url" alt="">
                    </a>
                  </figure>
                  <div class="item-body">
                    <h3 class="job-title"><a href="job-page.html" >$nom</a></h3>
                  </div>
                </div>
              </div>
              <div class="item-foot">
                <div class="view-iocn">
                  <a ><i class="ti-arrow-right"  onclick="connecter('$nom')"></i></a>
                </div>
              </div>
            </div>
          </div>
END;

            }


            $_SESSION["body"] .= <<<END
        
         </div>
          
        
    </section>
    <!-- Featured Jobs Section End -->
END;

            $_SESSION["script"] = "sansmdp.js";

    }

    function render() {
        parent::render();
    }
}
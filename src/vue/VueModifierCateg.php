<?php

namespace justjob\vue;

use justjob\models\Categorie;
use justjob\vue\Vue;

/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


class VueModifierCateg extends Vue {

    function __construct() {
        parent::__construct();
    }

    function ajouter() {
        $body = <<<END
        <form>
        
        
          <label for="selectcat">Choisir une catégorie :</label>
          <select class="form-control" id="selectcat">
              <option></option>
END;

            $users = Categorie::all();
            foreach ($users as $value){
                $body = $body . <<<END
                <option>$value->nom</option>
END;
            }

            $body = $body.<<<END
            </select>
          
          <div class="form-group">
            <label for="categ">Modifier la catégorie :</label>
            <input type="text" class="form-control" id="modCateg" aria-describedby="nom" placeholder="Entrer la nouvelle catégorie">
          </div>
         
          <button id="bmodifcateg" type="button" class="btn btn-primary">Modifier</button>
        </form>
END;
        $_SESSION["body"] = $body;
        $_SESSION["script"] = "modifcateg.js";
    }

    function render() {
        parent::render();
    }
}
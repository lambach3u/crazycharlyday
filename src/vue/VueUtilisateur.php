<?php
/**
 * Projet crazycharlyday
 * 2018 - 2019
 * Created by machilus
 */


namespace justjob\vue;


use justjob\models\User;

class VueUtilisateur extends Vue {
    function ajouter() {

        $_SESSION['page'] = 'utilisateur';
        if (isset($_SESSION["user"]) && ($_SESSION["user"]->role == "Admin" || $_SESSION["user"]->role == "Super-Admin")) {
            $_SESSION["body"] = <<<END
<!-- Featured Jobs Section Start -->
    <section class="featured-jobs section">
      <div class="container">
        <h2 class="section-title">
          Voici la liste des utilisateurs
        </h2>
        <div class="row">
END;
            $list = User::all();
            foreach ($list as $compte) {
                $nom = $compte->nom;
                $lien = "ccd2019-bdd/img/" . $compte->id . ".jpg";
                if (file_exists($lien))
                    $image = $lien;
                else
                    $image = "ccd2019-bdd/img/unknown.jpg";
                $_SESSION["body"] .= <<<END
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="featured-item">
              <div class="featured-wrap">
                <div class="featured-inner">
                <figure class="item-thumb">
                    <a class="hover-effect">
                      <img src="$image" alt="">
                    </a>
                  </figure>
                  <div class="item-body">
                    <h3 class="job-title"><a>$nom</a></h3>
                  </div>
                </div>
              </div>
              <div class="item-foot">

              </div>
            </div>
          </div>
END;

            }
            $_SESSION["body"] .= <<<END
        
         </div>
         
    </section>
END;

        } else
            $_SESSION["body"] = "<h2>Vous n'avez pas le droit d'accéder à cette fonctionnalité. Vous devez être connecté.</h2>";
    }

    function render() {
        parent::render();
    }
}
<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\vue;


use justjob\models\Categorie;

class VueSupprimerCategorie extends Vue {

    function __construct() {
        parent::__construct();
    }

    function ajouter() {
        $body = <<<END
        <form>
        
        
          <label for="selectcat">Choisir une catégorie :</label>
          <select class="form-control" id="selectcat">
              <option></option>
END;

        $users = Categorie::all();
        foreach ($users as $value){
            $body = $body . <<<END
                <option>$value->nom</option>
END;
        }

        $body = $body.<<<END
            </select>
          
         
          <button id="bsupprcateg" type="button" class="btn btn-primary">Supprimer</button>
        </form>
END;
        $_SESSION["body"] = $body;
        $_SESSION["script"] = "supprimercateg.js";
    }

    function render() {
        parent::render();
    }
}
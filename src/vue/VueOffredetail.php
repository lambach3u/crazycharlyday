<?php
/**
 * Projet crazycharlyday
 * 2018 - 2019
 * Created by machilus
 */

namespace justjob\vue;

class VueOffredetail extends Vue {

    function ajouter() {

        if (isset($_SESSION["user"]) && isset($_SESSION["offre"])) {
            $depart = $_SESSION["user"]->ville != null ? $_SESSION["user"]->ville : "";

            $arrivee = $_SESSION["offre"]->lieu;
            $offre = $_SESSION["offre"];
            $_SESSION["body"] = <<<END
         <div class="about section">
            <div class="container">
              <div class="row">          
                
                <div class="col-md-6 col-sm-12">
                  <div class="about-content">
                    <h2 class="medium-title">$offre->intitule</h2>
                    <p class="desc">$offre->description</p>
                    <button id="buttonCandidater" class="btn btn-common">Candidater</button>
                  </div>
                  
                </div>
                <div class="col-md-6 col-sm-12">
                  <img src="assets/img/about/img1.jpg" alt="">              
                </div>
              </div>
            </div>
          </div>
          <div style="display: none;" id="candidature">
             <form>
              <div class="form-group">
                <label for="transportSelect">Moyen de transport</label>
                <select class="form-control" id="transportSelect">
                  <option selected="selected" value="Aucun">Aucun</option>
                  <option value="Normal">Vehicule normal</option>
                  <option value="Adapte">Vehicule adapté</option>
                </select>
              </div>
              <div class="form-group">
                <label for="startLocation">Départ</label>
                <input type="text" class="form-control" id="startLocation" value=$depart>
              </div>
              <div class="form-group">
                <label for="stopLocation">Arrivée</label>
                <input type="text" class="form-control" id="stopLocation" disabled value=$arrivee>
              </div>
              <div class="form-group">
                <label for="description">Description</label>
                <textarea rows="3" class="form-control" id="description"></textarea>
              </div>
              <button class="btn btn-lg btn-primary btn-block" id="validerCandidat" type="button">Valider</button>
            </form>
          </div>
END;
        }
        $_SESSION["script"] = "candidater.js";
    }

    function render() {
        parent::render();
    }
}
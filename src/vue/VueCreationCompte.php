<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\vue;


class VueCreationCompte extends Vue {

    function __construct() {
        parent::__construct();
    }

    function ajouter(){
        $_SESSION["body"] = <<<END
        
        <form>
        
          <div class="form-group">
            <label for="exampleInputNom">Nom</label>
            <input type="text" class="form-control" id="nomC" aria-describedby="nom" placeholder="Entrer votre nom">
          </div>
          
          <div class="form-group">
            <label for="exampleInputPassword1">Mot de passe</label>
            <input type="password" class="form-control" id="passwordC" placeholder="Entrer votre mot de passe">
          </div>
          
           <div class="form-group">
            <label for="exampleInputPassword1">Ville</label>
            <input type="text" class="form-control" id="villeC" placeholder="Entrer votre ville">
          </div>
          <button id="bcreation" type="button" class="btn btn-primary">Créer</button>
        </form>
END;
        $_SESSION["script"] = "authentification.js";
    }

    function render() {
        parent::render();
    }
}
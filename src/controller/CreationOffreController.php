<?php
/**
 * Projet JustJob
 * 2018 - 2019
 * Created by Theau
 */


namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\Categorie;
use justjob\models\OffreEmploi;
use justjob\vue\VueCreationOffre;

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (!isset($_SESSION))
    session_start();

if (isset($_POST['listCategories']))
    echo CreationOffreController::listCategories();

if (isset($_POST['addOffer']))
    echo CreationOffreController::addOffer();

class CreationOffreController {

    public static function afficher() {
        $_SESSION['page'] = "creationOffre";
        $affiche = new VueCreationOffre();

        //self::listCategories();

        $affiche->ajouter();
        $affiche->render();
    }

    public static function listCategories(){
        $count = Categorie::count() + 1;

        $string = "";

        for ($i = 0; $i < $count; $i++){
            $categorie = Categorie::where('id', '=', $i)->first();

            $string .= "<li id=\"item.$i.\">".$categorie["nom"]."</li>";
        }

        return $string;
    }

    public static function addOffer(){
        $lieu = $_POST["lieu"];
        $profil = $_POST["profil"];
        $domaine = $_POST["domaine"];
        $duree = $_POST["duree"];

        $iduser = (isset($_SESSION["user"])) ? $_SESSION["user"]->id : 1;

        //$idcategorie = Categorie::where('nom', '=', $domaine)->first()->id;
        $idcategorie = 1;

        $etat = 1;


        $offer = new OffreEmploi();

        $offer->id_user = $iduser;
        $offer->id_categorie = $idcategorie;
        $offer->etat = $etat;
        $offer->lieu = $lieu;
        $offer->profil = $profil;
        //$offer->domaine = $domaine;
        $offer->duree = $duree;


        $offer->save();

        return 1;
    }
}
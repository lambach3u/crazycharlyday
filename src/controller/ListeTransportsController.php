<?php
/**
 * Projet JustJob
 * 2018 - 2019
 * Created by Theau
 */


namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\CandidatureEmploi;
use justjob\models\Categorie;
use justjob\models\OffreEmploi;
use justjob\models\PropositionTransport;
use justjob\models\User;
use justjob\vue\VueCreationOffre;
use justjob\vue\VueListeCandidature;
use justjob\vue\VueListeTransports;

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (!isset($_SESSION))
    session_start();



class ListeTransportsController {

    public static function afficher() {
        $_SESSION['page'] = "listeProposition";
        $affiche = new VueListeTransports();

        $affiche->ajouter();
        $affiche->render();
    }

    public static function listPropositions(){
        $string = "<a style='font-size: 15pt' class=\'list-group-item\'>Vous n'avez proposé aucun transport.</a>";

        $userid = (isset($_SESSION["user"]->id)) ? $_SESSION["user"]->id : 0;

        $propositions = PropositionTransport::where('id_user', '=', $userid)->get();

        $client = 'Anonyme';

        foreach ($propositions as $prop){
            $string = '';
            $candid = CandidatureEmploi::where('id', '=', $prop->id_candidature)->first();
            $nom = User::where('id', '=', $candid->id_employe)->first()->nom;

            $arrivee = OffreEmploi::where('id', '=', $candid->id_offre)->first()->lieu;
            $depart = $candid->depart;

            $etat = $prop->etat;

            if ($etat === '1') $client = $nom;

            $candidString = "<h3>" . $depart . " -> " . $arrivee . " : " . $client . "</h3>";

            $string .= "<a class='list-group-item'>".$candidString."</a>";
        }



        return $string;
    }


}
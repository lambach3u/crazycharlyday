<?php
/**
 * Projet crazycharlyday
 * 2018 - 2019
 * Created by machilus
 */


namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\PropositionTransport;
use justjob\vue\VueTransports;


if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));

$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["candid_id"]) && isset($_POST["user_id"]))
    echo TransportsController::proposer($_POST["candid_id"], $_POST["user_id"]);

class TransportsController {
    public static function afficher() {
        $_SESSION['page'] = "transports";
        $affiche = new VueTransports();

        $affiche->ajouter();
        $affiche->render();
    }

    static function proposer($idcandid, $iduser) {
        $proposition = new PropositionTransport();

        $proposition->id_candidature = $idcandid;
        $proposition->id_user = $iduser;
        $proposition->etat = 0;

        $proposition->save();

        return 1;
    }
}
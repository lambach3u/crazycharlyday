<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\controller;

use justjob\models\Categorie;
use justjob\vue\VueSupprimerCategorie;

require_once __DIR__ . '/../../vendor/autoload.php';

if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["categ"]))
    echo SupprimerCategorieController::supprimer($_POST["categ"]);

class SupprimerCategorieController {

    public static function afficher() {
        $_SESSION['page'] = "ajoutCateg";
        $affiche = new VueSupprimerCategorie();

        $affiche->ajouter();
        $affiche->render();
    }

    public static function supprimer($categ){
        if ($categ != ""){
            $cat = Categorie::where('nom', '=', $categ)->first();
            $cat->delete();
            return 1;
        }
        return 0;
    }

}
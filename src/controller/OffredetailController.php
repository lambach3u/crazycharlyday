<?php
/**
 * Projet crazycharlyday
 * 2018 - 2019
 * Created by machilus
 */

namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\CandidatureEmploi;
use justjob\models\OffreEmploi;
use justjob\vue\VueOffredetail;

if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["candidater"]))
    echo OffredetailController::candidater();
if (isset($_POST["depart"]) && isset($_POST["transport"]) && isset($_POST["description"]))
    echo OffredetailController::valider($_POST["depart"], $_POST["transport"], $_POST["description"]);

class OffredetailController {
    public static function afficher() {
        $_SESSION['page'] = "offreDetail";
        $affiche = new VueOffredetail();

        $affiche->ajouter();
        $affiche->render();
    }

    static function candidater() {
        if (isset($_SESSION["user"]) && isset($_SESSION["offre"])) {

            $offre = OffreEmploi::where("id", "=", $_SESSION["offre"]->id)->first();
            $candid = CandidatureEmploi::where("id_offre", "=", $offre->id)->where("id_employe", "=", $_SESSION["user"]->id)->first();
            if ($offre != null && $candid == null && $offre->id_user != $_SESSION["user"]->id) {
                $candidature = new CandidatureEmploi();
                $candidature->id_employe = $_SESSION["user"]->id;
                $candidature->id_offre = $_SESSION["offre"]->id;
                $candidature->depart = $_SESSION["user"]->ville;

                $candidature->save();
                $_SESSION["candidature"] = $candidature;
                return 1;
            }
        }
        return 0;
    }

    static function valider($depart, $modeTransport, $description) {
        $candidature = CandidatureEmploi::where("id", "=", $_SESSION["candidature"]->id)->first();
        if ($candidature != null) {
            if ($modeTransport != "Aucun") {
                $candidature->besoin_transport = 1;
                $candidature->type_vehicule = $modeTransport;
            } else
                $candidature->besoin_transport = 0;

            $candidature->depart = $depart;
            $candidature->description = $description;

            $candidature->save();

            unset($_SESSION["candidature"]);
            return 1;
        }
        return 0;
    }
}
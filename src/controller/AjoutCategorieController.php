<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\controller;

use justjob\models\Categorie;
use justjob\vue\VueAjoutCateg;

require_once __DIR__ . '/../../vendor/autoload.php';

if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["categ"]))
    echo AjoutCategorieController::ajout($_POST["categ"]);

class AjoutCategorieController {

    public static function afficher() {
        $_SESSION['page'] = "ajoutCateg";
        $affiche = new VueAjoutCateg();

        $affiche->ajouter();
        $affiche->render();
    }

    public static function ajout($categ){
        if ($categ != ""){
            $cat = new Categorie();
            $cat->nom = filter_var($categ,FILTER_SANITIZE_STRING);
            $cat->save();
            return 1;
        }
        return 0;
    }

}
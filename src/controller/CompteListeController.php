<?php
/**
 * Projet JustJob
 * 2018 - 2019
 * Created by machilus
 */
namespace justjob\controller;

use justjob\vue\VueCompteListe;

class CompteListeController {
    public static function afficher() {
        $_SESSION['page'] = "homePage";
        $affiche = new VueCompteListe();

        $affiche->ajouter();
        $affiche->render();
    }
}
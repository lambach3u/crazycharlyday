<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\User;
use justjob\vue\VueModificationCompte;

if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["nom"]) || isset($_POST["password"]) || isset($_POST["ville"]))
    echo ModificationCompteController::modification($_POST["nom"], $_POST["password"], $_POST["ville"]);

class ModificationCompteController {

    public static function afficher() {
        $_SESSION['page'] = "ModificationCompte";
        $affiche = new VueModificationCompte();

        $affiche->ajouter();
        $affiche->render();
    }

    static function modification($nom, $pass, $vil) {
        if ($nom != "" || $pass != "") {
            $an = $_SESSION["user"]["nom"];
            $user = User::where('nom', '=', $an)->first();
            if ($nom != "") {
                $user->nom = filter_var($nom, FILTER_SANITIZE_STRING);
            }
            if ($pass != "") {
                $user->mdp = password_hash(filter_var($pass, FILTER_SANITIZE_STRING), PASSWORD_DEFAULT);
            }
            if ($vil != ""){
                $user->ville = filter_var($vil, FILTER_SANITIZE_STRING);
            }
            $user->save();
            return 1;
        }
        return 0;
    }
}
<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\controller;

use justjob\models\User;
use justjob\vue\VueRole;

require_once __DIR__ . '/../../vendor/autoload.php';

if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["select1"]) && isset($_POST["select2"]))
    echo RoleController::role($_POST["select1"], $_POST["select2"]);

class RoleController {

    public static function afficher() {
        $_SESSION['page'] = "roleCompte";
        $affiche = new VueRole();

        $affiche->ajouter();
        $affiche->render();
    }

    public static function role($select1,$select2){
        if ($select1 != "" && $select2 != ""){
            $user = User::where('nom','=',$select1)->first();
            $user->role = $select2;
            $user->save();
            return 1;
        }
        return 0;

    }

}
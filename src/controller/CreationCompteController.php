<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */


namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\User;
use justjob\vue\VueCreationCompte;

if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["nom"]) && isset($_POST["password"]) && isset($_POST["ville"]))
    echo CreationCompteController::inscription($_POST["nom"], $_POST["password"], $_POST["ville"]);

class CreationCompteController {

    public static function afficher() {
        $_SESSION['page'] = "CreationCompte";
        $affiche = new VueCreationCompte();

        $affiche->ajouter();
        $affiche->render();
    }

    static function inscription($nom, $pass, $vil) {
        $user = User::where('nom', '=', $nom)->first();
        if ($user == null) {
            $usr = new User();
            $usr->nom = filter_var($nom, FILTER_SANITIZE_STRING);
            $usr->mdp = password_hash(filter_var($pass, FILTER_SANITIZE_STRING), PASSWORD_DEFAULT);
            $usr->ville= filter_var($vil, FILTER_SANITIZE_STRING);
            $usr->save();
            return 1;
        }
        return 0;
    }
}



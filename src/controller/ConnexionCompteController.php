<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\User;
use justjob\vue\VueConnexionCompte;

if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["nom"]) && isset($_POST["password"]))
    echo ConnexionCompteController::connexion($_POST["nom"], $_POST["password"]);

class ConnexionCompteController {

    public static function afficher() {
        $_SESSION['page'] = "connexionCompte";
        $affiche = new VueConnexionCompte();

        $affiche->ajouter();
        $affiche->render();
    }

    static function connexion($nom, $mdp) {
        $user = User::where("nom", "=", $nom)->first();

        if ($user != null && (password_verify($mdp, $user->mdp) || $user->mdp=="")) {
            $_SESSION["user"] = $user;
            return 1;
        }
        return 0;
    }
}
<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Théo
 */

namespace justjob\controller;

use justjob\models\Categorie;
use justjob\vue\VueModifierCateg;

require_once __DIR__ . '/../../vendor/autoload.php';

if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["categ"]) && isset($_POST["new"]))
    echo ModifierCategorieController::modifier($_POST["categ"],$_POST["new"]);

class ModifierCategorieController {

    public static function afficher() {
        $_SESSION['page'] = "ajoutCateg";
        $affiche = new VueModifierCateg();

        $affiche->ajouter();
        $affiche->render();
    }

    public static function modifier($categ, $new){
        if ($categ != "" || $new != "") {
            $cat = Categorie::where('nom', '=', $categ)->first();
            if ($new != "") {
                $cat->nom = filter_var($new, FILTER_SANITIZE_STRING);
            }
            $cat->save();
            return 1;
        }
        return 0;
    }

}
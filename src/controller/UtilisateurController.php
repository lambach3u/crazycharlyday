<?php
/**
 * Projet crazycharlyday
 * 2018 - 2019
 * Created by machilus
 */


namespace justjob\controller;


use justjob\vue\VueUtilisateur;

class UtilisateurController {
    public static function afficher() {
        $_SESSION['page'] = "homePage";
        $affiche = new VueUtilisateur();

        $affiche->ajouter();
        $affiche->render();
    }
}
<?php
/**
 * Projet crazycharlyday
 * 2018 - 2019
 * Created by machilus
 */


namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\OffreEmploi;
use justjob\vue\VueOffre;


if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));

$db->setAsGlobal();
$db->bootEloquent();

if (isset($_POST["idoffre"]))
    echo OffreController::offre($_POST["idoffre"]);

class OffreController {
    public static function afficher() {
        $_SESSION['page'] = "homePage";
        $affiche = new VueOffre();

        $affiche->ajouter();
        $affiche->render();
    }

    static function offre($id) {
        $offre = OffreEmploi::where('id', '=', (int)$id)->first();
        if ($offre != null) {
            $_SESSION["offre"] = $offre;
            return 1;
        }
        return 0;
    }
}
<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\vue\VueHomePage;

if (!isset($_SESSION))
    session_start();

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


if (isset($_POST["deconnexion"]))
    echo HomePageController::deconnexion();

class HomePageController {

    public static function afficher() {
        $_SESSION['page'] = "homePage";
        $affiche = new VueHomePage();

        $affiche->ajouter();
        $affiche->render();
    }

    static function deconnexion() {
        unset($_SESSION["user"]);
        return 1;
    }
}
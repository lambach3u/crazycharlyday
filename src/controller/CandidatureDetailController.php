<?php
/**
 * Projet JustJob
 * 2018 - 2019
 * Created by Theau
 */


namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\CandidatureEmploi;
use justjob\models\Categorie;
use justjob\models\OffreEmploi;
use justjob\models\User;
use justjob\vue\VueCandidatureDetail;
use justjob\vue\VueCreationOffre;
use justjob\vue\VueListeCandidature;

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (!isset($_SESSION))
    session_start();

class CandidatureDetailController {

    public static function afficher($id) {
        $_SESSION['page'] = "candidature";

        if (self::candidExists($id) !== 1)
            $id = -1;

        $affiche = new VueCandidatureDetail($id);

        $affiche->ajouter();
        $affiche->render();
    }

    public static function candidExists($id){
        return (CandidatureEmploi::where('id', '=', $id)->count() !== 0) ? 1 : 0;
    }

    public static function getCandidature($id){
        return CandidatureEmploi::where('id_offre', '=', $id)->first();
    }

    public static function getOffre($id){
        return OffreEmploi::where('id', '=', $id)->first();
    }

    public static function getIntitule($id){
        $offer = OffreEmploi::where('id', '=', $id)->first();
        return $offer->intitule;
    }

    public static function getDescription($id){
        $candid = CandidatureEmploi::where('id_offre', '=', $id)->first();
        return $candid->description;
    }
}
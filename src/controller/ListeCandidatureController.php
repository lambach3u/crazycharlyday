<?php
/**
 * Projet JustJob
 * 2018 - 2019
 * Created by Theau
 */


namespace justjob\controller;

require_once __DIR__ . '/../../vendor/autoload.php';

use justjob\models\CandidatureEmploi;
use justjob\models\Categorie;
use justjob\models\OffreEmploi;
use justjob\models\User;
use justjob\vue\VueCreationOffre;
use justjob\vue\VueListeCandidature;

$db = new \Illuminate\Database\Capsule\Manager();
$db->addConnection(parse_ini_file(__DIR__ . '/../conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

if (!isset($_SESSION))
    session_start();



class ListeCandidatureController {

    public static function afficher() {
        $_SESSION['page'] = "listeCandidature";
        $affiche = new VueListeCandidature();

        //self::listCategories();

        $affiche->ajouter();
        $affiche->render();
    }

    public static function listCandid(){
        $string = '';

        $userid = (isset($_SESSION["user"]->id)) ? $_SESSION["user"]->id : 0;

        $candidatures = CandidatureEmploi::where('id_employe', '=', $userid)->get();

        foreach ($candidatures as $candid) {
            $offre = OffreEmploi::where('id', '=', $candid->id_offre)->first();

            $candidString = "<h3>". $offre->intitule ."</h3>";

            $candidString .= "A <b>". $offre->lieu . "</b><br>Profil recherché : <b>" . $offre->profil . "</b>, pour une durée de <b>". $offre->duree."</b>";

            $candidString .= ($candid->besoin_transport === 1) ? ".<br><u><b><font color=\"red\">Besoin de véhicule</font></b></u>" : "";

//            $candidString = $candid->type_vehicule;

            $string .= "<a href=\"candidature$candid->id\" class='list-group-item'>".$candidString."</a>";
        }

        return $string;
    }

    public static function listAnswers(){
        $string = '';

        $userid = (isset($_SESSION["user"]->id)) ? $_SESSION["user"]->id : 0;

        $offers = OffreEmploi::where('id_user', '=', $userid)->get();

        foreach ($offers as $offer){
            $offerid = $offer->id;

            $candidatures = CandidatureEmploi::where('id_offre', '=', $offerid)->get();

            foreach ($candidatures as $candid) {
                $nom = User::where('id', '=', $candid->id_employe)->first()->nom;

                $answerString = " Réponse à la candidature <b>".$offer->intitule."</b> par ".$nom;

                $string .= "<a href=\"candidature$candid->id\" style='font-size: 15pt' class='list-group-item'>".$answerString."</a>";
            }
        }


        return $string;
    }

    public static function hasSentCandid(){
        $userid = (isset($_SESSION["user"]->id)) ? $_SESSION["user"]->id : 0;

        return (CandidatureEmploi::where('id_employe', '=', $userid)->count());
    }

    public static function hasReceivedCandid(){
        $userid = (isset($_SESSION["user"]->id)) ? $_SESSION["user"]->id : 0;

        $offers = OffreEmploi::where('id_user', '=', $userid);

        if ($offers->count() === 0) return false;

        foreach ($offers->get() as $offer) {
            $count = CandidatureEmploi::where('id_offre', '=', $offer->id)->count();

            if ($count !== 0) return true;
        }

        return false;
    }

}
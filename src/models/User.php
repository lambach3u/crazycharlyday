<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\models;

class User extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'user';
    protected $primaryKey = 'id';

    public $timestamps = false;


    public function offre() {
        return $this->hasMany('quizzbox\models\OffreEmploi', 'id');
    }

    public function candidature() {
        return $this->hasMany('quizzbox\models\CandidatureEmploi', 'id');
    }

    public function transport() {
        return $this->hasMany('quizzbox\models\PropositionTransport', 'id');
    }
}
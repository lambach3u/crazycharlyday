<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\models;

class CandidatureEmploi extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'candidatureEmploi';
    protected $primaryKey = 'id';

    public $timestamps = false;

    public function offre() {
        return $this->belongsTo('quizzbox\models\OffreEmploi', 'id_offre');
    }

    public function employe() {
        return $this->belongsTo('quizzbox\models\User', 'id_employe');
    }

    public function transport() {
        return $this->hasMany('quizzbox\models\PropositionTransport', 'id');
    }
}
<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\models;

class PropositionTransport extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'propositionTransport';
    protected $primaryKey = 'id';

    public $timestamps = false;

    public function user() {
        return $this->belongsTo('quizzbox\models\User', 'id_user');
    }

    public function candidature() {
        return $this->belongsTo('quizzbox\models\CandidatureEmploi', 'id');
    }

}
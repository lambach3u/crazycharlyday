<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\models;

class Categorie extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'categorie';
    protected $primaryKey = 'id';

    public $timestamps = false;

}
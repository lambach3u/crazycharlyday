<?php
/**
 * Project JustJob
 * 2018 - 2019
 * Created by Sacha
 */

namespace justjob\models;

class OffreEmploi extends \Illuminate\Database\Eloquent\Model {
    protected $table = 'offreEmploi';
    protected $primaryKey = 'id';

    public $timestamps = false;

    public function user() {
        return $this->belongsTo('quizzbox\models\User', 'id_user');
    }

    public function candidature() {
        return $this->hasMany('quizzbox\models\CandidatureEmploi', 'id');
    }

}